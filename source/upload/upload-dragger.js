(function(angular) {

    function BwcUploadDraggerController($scope) {
        $scope.dragover = false

        /**
         * mehtods
         */
        window.onDrop = function(e) {
            e.preventDefault();
            e.returnValue = false;
            $scope.dragover = false;
            $scope.$ctrl.onFile(e.dataTransfer.files)
        }

        window.dragoverFn = function(e, flag) {
            e.preventDefault();
            e.returnValue = false;
            $scope.dragover = flag
        }

    }

    angular.module('bwc.component')
        .component('bwcUploadDragger', {
            template: `<div
                            class="el-upload-dragger"
                            ng-class="{
                                'is-dragover': dragover
                            }"
                            ondrop="onDrop(event)"
                            ondragover="dragoverFn(event,true)"
                            ondragleave="dragoverFn(event,false)"
                            ng-transclude
                        >
                        </div>`,
            controller: ['$scope', BwcUploadDraggerController],
            transclude: true,
            replace: true,
            bindings: {
                onFile: '=?'
            }
        });

})(angular);