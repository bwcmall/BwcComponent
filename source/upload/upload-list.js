(function(angular) {

    function BwcUploadListController($scope) {
        $scope.$ctrl.files = $scope.$ctrl.files ? $scope.$ctrl.files : []
            /**
             * mehtods
             */
        $scope.parsePercentage = function(val) {
            return parseInt(val, 10);
        }
        $scope.handleClick = function(file) {
            $scope.$ctrl.handlePreview && $scope.$ctrl.handlePreview(file);
        }

        $scope.type = $scope.$ctrl.listType === 'picture-card' ? 'circle' : 'line'
        $scope.strokeWidth = $scope.$ctrl.listType === 'picture-card' ? 6 : 2

    }

    angular.module('bwc.component')
        .component('bwcUploadList', {
            template: `<ul ng-class="['el-upload-list', 'el-upload-list--' + $ctrl.listType]">
                            <li
                                ng-repeat="file in $ctrl.files"
                                ng-class="['el-upload-list__item', 'is-' + file.status]"
                                key="{{file}}"
                            >
                            <img
                                class="el-upload-list__item-thumbnail"
                                ng-if="['picture-card', 'picture'].indexOf($ctrl.listType) > -1 && file.status === 'success'"
                                ng-src="{{file.url}}" alt=""
                            >
                            <a class="el-upload-list__item-name" ng-click="handleClick(file)">
                                <i class="el-icon-document"></i>{{file.name}}
                            </a>
                            <label ng-show="file.status === 'success'" class="el-upload-list__item-status-label">
                                <i ng-class="{
                                    'el-icon-circle-check': $ctrl.listType === 'text',
                                    'el-icon-check': ['picture-card', 'picture'].indexOf($ctrl.listType) > -1
                                }"></i>
                                <i class="el-icon-close" ng-click="$ctrl.handleRemove(file)"></i>
                            </label>
                            <span class="el-upload-list__item-actions"
                                ng-if="
                                $ctrl.listType === 'picture-card' &&
                                file.status === 'success'
                                "
                            >
                                <span
                                ng-if="
                                    $ctrl.handlePreview &&
                                    $ctrl.listType === 'picture-card'
                                "
                                ng-click="$ctrl.handlePreview(file)"
                                class="el-upload-list__item-preview"
                                >
                                <i class="el-icon-view"></i>
                                </span>
                                <span
                                class="el-upload-list__item-delete"
                                ng-click="$ctrl.handleRemove(file)"
                                >
                                <i class="el-icon-delete2"></i>
                                </span>
                            </span>
                            <bwc-progress
                                ng-if="file.status === 'uploading'"
                                type="{{type}}"
                                stroke-width="strokeWidth"
                                percentage="parsePercentage(file.percentage)">
                            </bwc-progress>
                            </li>
                        </ul>`,
            controller: ['$scope', BwcUploadListController],
            transclude: true,
            replace: true,
            bindings: {
                files: "=?",
                handlePreview: "=?",
                handleRemove: "=?",
                listType: "@?",
            }
        });

})(angular);