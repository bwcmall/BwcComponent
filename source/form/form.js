// (function(angular) {

//     function BwcFormController($scope) {
//         $scope.$ctrl.labelSuffix = $scope.$ctrl.labelSuffix ? $scope.$ctrl.labelSuffix : ''
//         $scope.$ctrl.showMessage = $scope.$ctrl.showMessage ? $scope.$ctrl.showMessage : true

//         $scope.fields = []

//         $scope.$on('bwc.form.addField', function(field) {
//             if (field) {
//                 $scope.fields.push(field);
//             }
//         });
//         /* istanbul ignore next */
//         $scope.$on('bwc.form.removeField', function(field) {
//             if (field.prop) {
//                 $scope.fields.splice($scope.fields.indexOf(field), 1);
//             }
//         });

//         $scope.$watch('$ctrl.rules', function() {
//             $scope.validate()
//         });

//         $scope.resetFields() {
//             $scope.fields.forEach(field => {
//                 field.resetField();
//             });
//         }
//         $scope.validate(callback) {
//             let valid = true;
//             let count = 0;
//             $scope.fields.forEach((field, index) => {
//                 field.validate('', errors => {
//                     if (errors) {
//                         valid = false;
//                     }
//                     if (typeof callback === 'function' && ++count === $scope.fields.length) {
//                         callback(valid);
//                     }
//                 });
//             });
//         }
//         $scope.validateField(prop, cb) {
//             var field = $scope.fields.filter(field => field.prop === prop)[0];
//             if (!field) { throw new Error('must call validateField with valid prop string!'); }

//             field.validate('', cb);
//         }

//     }

//     angular.module('bwc.component')
//         .component('bwcForm', {
//             template: `<form class="el-form" ng-class="[
//                             $ctrl.labelPosition ? 'el-form--label-' + $ctrl.labelPosition : '',
//                             { 'el-form--inline': $ctrl.inline }
//                         ]" ng-transclude>
//                         </form>`,
//             controller: ['$scope', BwcFormController],
//             transclude: true,
//             bindings: {
//                 model: "=?",
//                 rules: "=?",
//                 labelPosition: "@?",
//                 labelWidth: "@?",
//                 labelSuffix: "@?",
//                 inline: "=?",
//                 showMessage: "=?"
//             }
//         });

// })(angular);