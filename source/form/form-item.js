// (function(angular) {

//     function BwcFormItemController($scope) {
//         $scope.$ctrl.showMessage = $scope.$ctrl.showMessage ? $scope.$ctrl.showMessage : ''

//         //data
//         $scope.validateState = ''
//         $scope.validateMessage = ''
//         $scope.validateDisabled = false
//         $scope.validator = {}
//         $scope.isRequired = false

//         function noop() {}

//         function getPropByPath(obj, path) {
//             let tempObj = obj;
//             path = path.replace(/\[(\w+)\]/g, '.$1');
//             path = path.replace(/^\./, '');

//             let keyArr = path.split('.');
//             let i = 0;

//             for (let len = keyArr.length; i < len - 1; ++i) {
//                 let key = keyArr[i];
//                 if (key in tempObj) {
//                     tempObj = tempObj[key];
//                 } else {
//                     throw new Error('please transfer a valid prop path to form item!');
//                 }
//             }
//             return {
//                 o: tempObj,
//                 k: keyArr[i],
//                 v: tempObj[keyArr[i]]
//             };
//         }

//         //methods
//         $scope.validate(trigger, callback = noop) {
//             var rules = $scope.getFilteredRule(trigger);
//             if (!rules || rules.length === 0) {
//                 callback();
//                 return true;
//             }

//             $scope.validateState = 'validating';

//             var descriptor = {};
//             descriptor[$scope.$ctrl.prop] = rules;

//             var validator = new AsyncValidator(descriptor);
//             var model = {};

//             model[$scope.$ctrl.prop] = this.fieldValue;

//             validator.validate(model, { firstFields: true }, (errors, fields) => {
//                 this.validateState = !errors ? 'success' : 'error';
//                 this.validateMessage = errors ? errors[0].message : '';

//                 callback(this.validateMessage);
//             });
//         }
//         $scope.resetField() {
//             this.validateState = '';
//             this.validateMessage = '';

//             let model = this.form.model;
//             let value = this.fieldValue;
//             let path = $scope.$ctrl.prop;
//             if (path.indexOf(':') !== -1) {
//                 path = path.replace(/:/, '.');
//             }

//             let prop = getPropByPath(model, path);

//             if (Array.isArray(value) && value.length > 0) {
//                 this.validateDisabled = true;
//                 prop.o[prop.k] = [];
//             } else if (value !== '') {
//                 this.validateDisabled = true;
//                 prop.o[prop.k] = this.initialValue;
//             }
//         }
//         $scope.getRules() {
//             var formRules = this.form.rules;
//             var selfRuels = $scope.$ctrl.rules;

//             formRules = formRules ? formRules[$scope.$ctrl.prop] : [];

//             return [].concat(selfRuels || formRules || []);
//         }
//         $scope.getFilteredRule(trigger) {
//             var rules = this.getRules();

//             return rules.filter(rule => {
//                 return !rule.trigger || rule.trigger.indexOf(trigger) !== -1;
//             });
//         }
//         $scope.onFieldBlur() {
//             this.validate('blur');
//         }
//         $scope.onFieldChange() {
//             if (this.validateDisabled) {
//                 this.validateDisabled = false;
//                 return;
//             }

//             this.validate('change');
//         }

//         //computed
//         $scope.labelStyle = function() {
//             var ret = {};
//             if (this.form.labelPosition === 'top') return ret;
//             var labelWidth = this.labelWidth || this.form.labelWidth;
//             if (labelWidth) {
//                 ret.width = labelWidth;
//             }
//             return ret;
//         }
//         $scope.contentStyle = function() {
//             var ret = {};
//             if (this.form.labelPosition === 'top' || this.form.inline) return ret;
//             var labelWidth = this.labelWidth || this.form.labelWidth;
//             if (labelWidth) {
//                 ret.marginLeft = labelWidth;
//             }
//             return ret;
//         }
//         $scope.form = function() {
//             var parent = this.$parent;
//             while (parent.$options.componentName !== 'ElForm') {
//                 parent = parent.$parent;
//             }
//             return parent;
//         }
//         $scope.fieldValue = function() {
//             var model = this.form.model;
//             if (!model || !this.prop) { return; }

//             var path = this.prop;
//             if (path.indexOf(':') !== -1) {
//                 path = path.replace(/:/, '.');
//             }

//             return getPropByPath(model, path).v;
//         }


//     }

//     angular.module('bwc.component')
//         .component('bwcFormItem', {
//             template: `<div class="el-form-item" :ng-lass="{
//                         'is-error': validateState === 'error',
//                         'is-validating': validateState === 'validating',
//                         'is-required': isRequired || $ctrl.required
//                     }">
//                         <label for="{{$ctrl.prop}}" class="el-form-item__label" style="{{labelStyle}}" ng-if="$ctrl.label">
//                         {{$ctrl.label + form().labelSuffix}}
//                         </label>
//                         <div class="el-form-item__content" style="{{contentStyle}}">
//                         <span ng-transclude></span>
//                         <div class="el-form-item__error" ng-if="validateState === 'error' && $ctrl.showMessage && form().showMessage">{{validateMessage}}</div>
//                         </div>
//                     </div>`,
//             controller: ['$scope', BwcFormItemController],
//             transclude: true,
//             bindings: {
//                 label: "@?",
//                 labelWidth: "@?",
//                 prop: "@?",
//                 required: "=?",
//                 rules: "=?",
//                 error: "@?",
//                 validateStatus: "@?",
//                 showMessage: "=?"
//             }
//         });

// })(angular);