(function (angular) {

    function CheckboxController($scope) {
        console.log($scope)



        //data
        $scope.selfModel = false
        $scope.focus = false

        $scope.model = ''
        $scope.setModel = function () {

        }
        $scope.getModel = function () {
            $scope.isGroup()
        }

        $scope.$watch('model',function(value){
            console.log('model')
            if($scope.isGroup()){

            }
        })

        //created
        $scope.$ctrl.checked && $scope.addToStore();


        $scope.isChecked = function () {
            console.log($scope.$parent.checked)
            if ({}.toString.call($scope.$parent.checked) === '[object Boolean]') {
                return $scope.$parent.checked;
            } else if (Array.isArray($scope.$parent.checked)) {
                return $scope.$parent.checked.indexOf($scope.$ctrl.label) > -1;
            } else if ($scope.$parent.checked !== null && $scope.$parent.checked !== undefined) {
                return $scope.$parent.checked === $scope.$ctrl.trueLabel;
            }
        }

        //method
        $scope.addToStore = function () {
            if (Array.isArray($scope.model) && $scope.model.indexOf($scope.$ctrl.label) === -1) {
                $scope.model.push($scope.$ctrl.label);
            } else {
                $scope.model = $scope.$ctrl.trueLabel || true;
            }
        }
        $scope.handleChange = function (ev) {
            this.$emit('change', ev);
            if (this.isGroup) {
                // this.$nextTick(_ => {
                //     this.dispatch('ElCheckboxGroup', 'change', [this._checkboxGroup.value]);
                // });
            }
        }
        $scope.isChecked = function () {
            if ({}.toString.call(this.model) === '[object Boolean]') {
                return this.model;
            } else if (Array.isArray(this.model)) {
                return this.model.indexOf(this.label) > -1;
            } else if (this.model !== null && this.model !== undefined) {
                return this.model === this.trueLabel;
            }
        }
        $scope.isGroup = function () {
            console.log($scope.$parent)
            // let parent = $scope.$parent;
            // while (parent) {
            //     if (parent.$options.componentName !== 'ElCheckboxGroup') {
            //         parent = parent.$parent;
            //     } else {
            //         this._checkboxGroup = parent;
            //         return true;
            //     }
            // }
            // return false;
        }
        $scope.store = function () {
            return this._checkboxGroup ? this._checkboxGroup.value : this.value;
        }
        //method end


    }



    angular.module('bwc.component')
        .component('bwcCheckbox', {
            template: `
            <label class="el-checkbox">
            {{$scope.isChecked}}
                <span class="el-checkbox__input"
                ng-class="{
                    'is-disabled': $ctrl.disabled,
                    'is-checked': isChecked(),
                    'is-indeterminate': $ctrl.indeterminate,
                    'is-focus': focus
                }"
                >
                <span class="el-checkbox__inner"></span>
                <input
                    ng-if="$ctrl.trueLabel || $ctrl.falseLabel"
                    class="el-checkbox__original"
                    type="checkbox"
                    name="{{$ctrl.name}}"
                    disabled="{{$ctrl.disabled}}"
                    true-value="{{$ctrl.trueLabel}}"
                    false-value="{{$ctrl.falseLabel}}"
                    ng-model="model"
                    ng-change="handleChange()"
                    ng-focus="focus = true"
                    ng-blur="focus = false">
                <input
                    v-else
                    class="el-checkbox__original"
                    type="checkbox"
                    ng-disabled="disabled"
                    value="{{$ctrl.label}}"
                    name="{{$ctrl.name}}"
                    ng-model="model"
                    ng-change="handleChange()"
                    ng-focus="focus = true"
                    ng-blur="focus = false">
                </span>
                <span class="el-checkbox__label" ng-transclude>
                </span>
            </label>
            `,
            controller: ['$scope', CheckboxController],
            transclude: true,
            replace: true,
            bindings: {
                value: "=?",
                label: "@?",
                indeterminate: "=?",
                disabled: "=?",
                checked: "=?",
                name: "@?",
                trueLabel: "=?",
                falseLabel: "=?",
                change: "&?"
            }
        });

})(angular);