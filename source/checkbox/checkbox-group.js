(function (angular) {

    function CheckboxGroupController($scope) {

        // $scope.$watch('$ctrl.value', function (value) {

        // })

    }



    angular.module('bwc.component')
        .component('bwcCheckboxGroup', {
            template: `<div class="el-checkbox-group" ng-transclude>
                        </div>
            `,
            controller: ['$scope', CheckboxGroupController],
            transclude: true,
            replace: true,
            bindings: {
                value: "=?"
            }
        });

})(angular);