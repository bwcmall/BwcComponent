(function(angular) {

    function BwcProgressController($scope) {
        $scope.$ctrl.type = $scope.$ctrl.type ? $scope.$ctrl.type : 'line'
        var typeArray = ['line', 'circle']
        if ($scope.$ctrl.type != 'line' && $scope.$ctrl.type != 'circle') {
            return
        }
        
        $scope.$ctrl.percentage = $scope.$ctrl.percentage ? $scope.$ctrl.percentage : 0
        if ($scope.$ctrl.percentage < 0) {
            $scope.$ctrl.percentage = 0
        }
        if ($scope.$ctrl.percentage > 100) {
            $scope.$ctrl.percentage = 100
        }
        $scope.$ctrl.strokeWidth = $scope.$ctrl.strokeWidth ? $scope.$ctrl.strokeWidth : 6
        $scope.$ctrl.textInside = $scope.$ctrl.textInside ? $scope.$ctrl.textInside : false
        $scope.$ctrl.width = $scope.$ctrl.width ? $scope.$ctrl.width : 126
        $scope.$ctrl.showText = $scope.$ctrl.showText ? $scope.$ctrl.showText : true

        /**
         * mehtods
         */
        $scope.barStyle = function() {
            var style = {};
            style.width = $scope.$ctrl.percentage + '%';
            return style;
        }
        $scope.relativeStrokeWidth = function() {
            return ($scope.$ctrl.strokeWidth / $scope.$ctrl.width * 100).toFixed(1);
        }
        $scope.trackPath = function() {
            var radius = parseInt(50 - parseFloat($scope.relativeStrokeWidth()) / 2, 10);
            return `M 50 50 m 0 -${radius} a ${radius} ${radius} 0 1 1 0 ${radius * 2} a ${radius} ${radius} 0 1 1 0 -${radius * 2}`;
        }
        $scope.perimeter = function() {
            var radius = 50 - parseFloat($scope.relativeStrokeWidth()) / 2;
            return 2 * Math.PI * radius;
        }
        $scope.circlePathStyle = function() {
            var perimeter = $scope.perimeter()
            return {
                strokeDasharray: `${perimeter}px,${perimeter}px`,
                strokeDashoffset: (1 - $scope.$ctrl.percentage / 100) * perimeter + 'px',
                transition: 'stroke-dashoffset 0.6s ease 0s, stroke 0.6s ease'
            };
        }
        $scope.stroke = function() {
            var ret;
            switch ($scope.$ctrl.status) {
                case 'success':
                    ret = '#13ce66';
                    break;
                case 'exception':
                    ret = '#ff4949';
                    break;
                default:
                    ret = '#20a0ff';
            }
            return ret;
        }
        $scope.iconClass = function() {
            if ($scope.$ctrl.type === 'line') {
                return $scope.$ctrl.status === 'success' ? 'el-icon-circle-check' : 'el-icon-circle-cross';
            } else {
                return $scope.$ctrl.status === 'success' ? 'el-icon-check' : 'el-icon-close';
            }
        }
        $scope.progressTextSize = function() {
            return $scope.$ctrl.type === 'line' ?
                12 + $scope.$ctrl.strokeWidth * 0.4 :
                $scope.$ctrl.width * 0.111111 + 2;
        }
    }

    angular.module('bwc.component')
        .directive('svgD', function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    element.attr('d', attrs.svgD)
                }
            };
        });

    angular.module('bwc.component')
        .component('bwcProgress', {
            template: `<div
                        class="el-progress"
                        ng-class="[
                        'el-progress--' + $ctrl.type,
                        $ctrl.status ? 'is-' + $ctrl.status : '',
                        {
                            'el-progress--without-text': !$ctrl.showText,
                            'el-progress--text-inside': $ctrl.textInside,
                        }
                        ]"
                    >
                        <div class="el-progress-bar" ng-if="$ctrl.type === 'line'">
                            <div class="el-progress-bar__outer" ng-style="{height: $ctrl.strokeWidth + 'px'}">
                                <div class="el-progress-bar__inner" ng-style="barStyle()">
                                <div class="el-progress-bar__innerText" ng-if="$ctrl.showText && $ctrl.textInside">{{$ctrl.percentage}}%</div>
                                </div>
                            </div>
                        </div>
                        <div ng-if="$ctrl.type !== 'line'" class="el-progress-circle" ng-style="{height: $ctrl.width + 'px', width: $ctrl.width + 'px'}" >
                            <svg viewBox="0 0 100 100">
                                <path class="el-progress-circle__track" svg-d="{{trackPath()}}" stroke="#e5e9f2" stroke-width="{{relativeStrokeWidth()}}" fill="none"></path>
                                <path class="el-progress-circle__path" svg-d="{{trackPath()}}" stroke-linecap="round" stroke="{{stroke()}}" stroke-width="{{relativeStrokeWidth()}}" fill="none" ng-style="circlePathStyle()"></path>
                            </svg>
                        </div>
                        <div
                        class="el-progress__text"
                        ng-if="$ctrl.showText && !$ctrl.textInside"
                        ng-style="{fontSize: progressTextSize() + 'px'}"
                        >
                            <span ng-if="!$ctrl.status">{{$ctrl.percentage}}%</span>
                            <i ng-if="$ctrl.status" ng-class="iconClass()"></i>
                        </div>
                    </div>`,
            controller: ['$scope', BwcProgressController],
            transclude: true,
            replace: true,
            bindings: {
                type: "@?",
                percentage: "=?",
                status: "@?",
                strokeWidth: "=?",
                textInside: "=?",
                width: "=?",
                showText: "=?"
            }
        });

})(angular);