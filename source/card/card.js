(function(angular) {

    function BwcCardController($scope) {
       
    }

    angular.module('bwc.component')
        .component('bwcCard', {
            template: ` <div class="el-card">
                            <div ng-show="$ctrl.showHeader" class="el-card__header" ng-transclude="header">
                                <div>{{ header }}</div>
                            </div>
                            <div class="el-card__body" :style="bodyStyle" ng-transclude>
                            </div>
                        </div>`,
            controller: ['$scope', BwcCardController],
            transclude: {
                header: "?header"
            },
            replace: true,
            bindings: {
                showHeader: "=?",
                bodyStyle: "=?"
            }
        });

})(angular);