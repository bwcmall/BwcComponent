(function() {
    'use strict';

    angular.module('demoApp', ['ngRoute', 'ui.bootstrap', 'bwc.component','ngAnimate'])

    .config(['$routeProvider', '$compileProvider', function($routeProvider, $compileProvider) {
        $routeProvider
            .when('/upload', {
                templateUrl: 'views/upload.html'
            })
            .when('/card', {
                controller: 'CardController',
                templateUrl: 'views/card.html'
            })
            .when('/layout', {
                controller: 'LayoutController',
                templateUrl: 'views/layout.html'
            })
            .when('/form', {
                controller: 'FormController',
                templateUrl: 'views/form.html'
            })
            .when('/progress', {
                controller: 'ProgressController',
                templateUrl: 'views/progress.html'
            })
            .when('/checkbox', {
                controller: 'CheckboxController',
                templateUrl: 'views/checkbox.html'
            })
            .otherwise({
                redirectTo: '/'
            });

        // testing issue #521
    }]);
})();