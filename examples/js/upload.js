(function() {
    'use strict';

    angular.module('demoApp')
        .controller('UploadController', ['$scope', function($scope) {

            $scope.fileList = [{ name: 'food.jpeg', url: 'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?imageMogr2/thumbnail/360x360/format/webp/quality/100' }, { name: 'food2.jpeg', url: 'https://fuss10.elemecdn.com/3/63/4e7f3a15429bfda99bce42a18cdd1jpeg.jpeg?imageMogr2/thumbnail/360x360/format/webp/quality/100' }]

            $scope.handleRemove = function(file, fileList) {
                console.log(file, fileList);
            }
            $scope.handlePreview = function(file) {
                console.log(file);
            }

            //=============>

            $scope.imageUrl = ""

            $scope.handleAvatarSuccess = function(res, file) {
                console.log(111)
                $scope.imageUrl = URL.createObjectURL(file.raw);
            }
            $scope.beforeAvatarUpload = function(file) {
                return true
            }

            //==============>
            $scope.handlePictureCardPreview = function(file) {
                this.dialogImageUrl = file.url;
                this.dialogVisible = true;
                console.log(1)
            }

            $scope.handleSuccess = function(response, file, fileList) {
                console.log(222)
            }


        }]);

}());