/**
 * @license Bwcmall Angular components package v1.0.0
 * License: MIT
 */
(function(angular) {
    'use strict';

    angular.module('bwc.component', [])
        .constant('bwcConfig', {
            baseUrl: '/'
        });

})(angular);
(function(angular) {

    function BwcButtonController($scope) {
        $scope.$ctrl.type = $scope.$ctrl.type ? $scope.$ctrl.type : 'default'
        $scope.$ctrl.icon = $scope.$ctrl.icon ? $scope.$ctrl.icon : ''
        $scope.$ctrl.nativeType = $scope.$ctrl.nativeType ? $scope.$ctrl.nativeType : 'button'

    }

    angular.module('bwc.component')
        .component('bwcButton', {
            template: `<button ng-disabled="$ctrl.disabled" class="el-button" ng-click="handleClick()" autofocus="{{$ctrl.autofocus}}" type="{{$ctrl.nativeType}}"
                            ng-class="[
                            $ctrl.type ? 'el-button--' + $ctrl.type : '',
                            $ctrl.size ? 'el-button--' + $ctrl.size : '',
                            {
                                'is-disabled': $ctrl.disabled,
                                'is-loading': $ctrl.loading,
                                'is-plain': $ctrl.plain
                            }
                            ]">
                            <i class="el-icon-loading" ng-if="$ctrl.loading"></i>
                            <i ng-class="'el-icon-' + $ctrl.icon" ng-if="$ctrl.icon && !$ctrl.loading"></i>
                            <span ng-transclude></span>
                        </button>`,
            controller: ['$scope', BwcButtonController],
            transclude: true,
            replace: true,
            bindings: {
                autofocus: "=?",
                loading: "=?",
                disabled: "=?",
                plain: "=?",
                autofocus: "=?",
                type: "@?",
                icon: "@?",
                size: "@?",
                nativeType: "=?"
            }
        });

})(angular);
(function(angular) {

    function BwcCardController($scope) {
       
    }

    angular.module('bwc.component')
        .component('bwcCard', {
            template: ` <div class="el-card">
                            <div ng-show="$ctrl.showHeader" class="el-card__header" ng-transclude="header">
                                <div>{{ header }}</div>
                            </div>
                            <div class="el-card__body" :style="bodyStyle" ng-transclude>
                            </div>
                        </div>`,
            controller: ['$scope', BwcCardController],
            transclude: {
                header: "?header"
            },
            replace: true,
            bindings: {
                showHeader: "=?",
                bodyStyle: "=?"
            }
        });

})(angular);
(function (angular) {

    function CheckboxGroupController($scope) {

        // $scope.$watch('$ctrl.value', function (value) {

        // })

    }



    angular.module('bwc.component')
        .component('bwcCheckboxGroup', {
            template: `<div class="el-checkbox-group" ng-transclude>
                        </div>
            `,
            controller: ['$scope', CheckboxGroupController],
            transclude: true,
            replace: true,
            bindings: {
                value: "=?"
            }
        });

})(angular);
(function (angular) {

    function CheckboxController($scope) {
        console.log($scope)



        //data
        $scope.selfModel = false
        $scope.focus = false

        $scope.model = ''
        $scope.setModel = function () {

        }
        $scope.getModel = function () {
            $scope.isGroup()
        }

        $scope.$watch('model',function(value){
            console.log('model')
            if($scope.isGroup()){

            }
        })

        //created
        $scope.$ctrl.checked && $scope.addToStore();


        $scope.isChecked = function () {
            console.log($scope.$parent.checked)
            if ({}.toString.call($scope.$parent.checked) === '[object Boolean]') {
                return $scope.$parent.checked;
            } else if (Array.isArray($scope.$parent.checked)) {
                return $scope.$parent.checked.indexOf($scope.$ctrl.label) > -1;
            } else if ($scope.$parent.checked !== null && $scope.$parent.checked !== undefined) {
                return $scope.$parent.checked === $scope.$ctrl.trueLabel;
            }
        }

        //method
        $scope.addToStore = function () {
            if (Array.isArray($scope.model) && $scope.model.indexOf($scope.$ctrl.label) === -1) {
                $scope.model.push($scope.$ctrl.label);
            } else {
                $scope.model = $scope.$ctrl.trueLabel || true;
            }
        }
        $scope.handleChange = function (ev) {
            this.$emit('change', ev);
            if (this.isGroup) {
                // this.$nextTick(_ => {
                //     this.dispatch('ElCheckboxGroup', 'change', [this._checkboxGroup.value]);
                // });
            }
        }
        $scope.isChecked = function () {
            if ({}.toString.call(this.model) === '[object Boolean]') {
                return this.model;
            } else if (Array.isArray(this.model)) {
                return this.model.indexOf(this.label) > -1;
            } else if (this.model !== null && this.model !== undefined) {
                return this.model === this.trueLabel;
            }
        }
        $scope.isGroup = function () {
            console.log($scope.$parent)
            // let parent = $scope.$parent;
            // while (parent) {
            //     if (parent.$options.componentName !== 'ElCheckboxGroup') {
            //         parent = parent.$parent;
            //     } else {
            //         this._checkboxGroup = parent;
            //         return true;
            //     }
            // }
            // return false;
        }
        $scope.store = function () {
            return this._checkboxGroup ? this._checkboxGroup.value : this.value;
        }
        //method end


    }



    angular.module('bwc.component')
        .component('bwcCheckbox', {
            template: `
            <label class="el-checkbox">
            {{$scope.isChecked}}
                <span class="el-checkbox__input"
                ng-class="{
                    'is-disabled': $ctrl.disabled,
                    'is-checked': isChecked(),
                    'is-indeterminate': $ctrl.indeterminate,
                    'is-focus': focus
                }"
                >
                <span class="el-checkbox__inner"></span>
                <input
                    ng-if="$ctrl.trueLabel || $ctrl.falseLabel"
                    class="el-checkbox__original"
                    type="checkbox"
                    name="{{$ctrl.name}}"
                    disabled="{{$ctrl.disabled}}"
                    true-value="{{$ctrl.trueLabel}}"
                    false-value="{{$ctrl.falseLabel}}"
                    ng-model="model"
                    ng-change="handleChange()"
                    ng-focus="focus = true"
                    ng-blur="focus = false">
                <input
                    v-else
                    class="el-checkbox__original"
                    type="checkbox"
                    ng-disabled="disabled"
                    value="{{$ctrl.label}}"
                    name="{{$ctrl.name}}"
                    ng-model="model"
                    ng-change="handleChange()"
                    ng-focus="focus = true"
                    ng-blur="focus = false">
                </span>
                <span class="el-checkbox__label" ng-transclude>
                </span>
            </label>
            `,
            controller: ['$scope', CheckboxController],
            transclude: true,
            replace: true,
            bindings: {
                value: "=?",
                label: "@?",
                indeterminate: "=?",
                disabled: "=?",
                checked: "=?",
                name: "@?",
                trueLabel: "=?",
                falseLabel: "=?",
                change: "&?"
            }
        });

})(angular);
// (function(angular) {

//     function BwcFormItemController($scope) {
//         $scope.$ctrl.showMessage = $scope.$ctrl.showMessage ? $scope.$ctrl.showMessage : ''

//         //data
//         $scope.validateState = ''
//         $scope.validateMessage = ''
//         $scope.validateDisabled = false
//         $scope.validator = {}
//         $scope.isRequired = false

//         function noop() {}

//         function getPropByPath(obj, path) {
//             let tempObj = obj;
//             path = path.replace(/\[(\w+)\]/g, '.$1');
//             path = path.replace(/^\./, '');

//             let keyArr = path.split('.');
//             let i = 0;

//             for (let len = keyArr.length; i < len - 1; ++i) {
//                 let key = keyArr[i];
//                 if (key in tempObj) {
//                     tempObj = tempObj[key];
//                 } else {
//                     throw new Error('please transfer a valid prop path to form item!');
//                 }
//             }
//             return {
//                 o: tempObj,
//                 k: keyArr[i],
//                 v: tempObj[keyArr[i]]
//             };
//         }

//         //methods
//         $scope.validate(trigger, callback = noop) {
//             var rules = $scope.getFilteredRule(trigger);
//             if (!rules || rules.length === 0) {
//                 callback();
//                 return true;
//             }

//             $scope.validateState = 'validating';

//             var descriptor = {};
//             descriptor[$scope.$ctrl.prop] = rules;

//             var validator = new AsyncValidator(descriptor);
//             var model = {};

//             model[$scope.$ctrl.prop] = this.fieldValue;

//             validator.validate(model, { firstFields: true }, (errors, fields) => {
//                 this.validateState = !errors ? 'success' : 'error';
//                 this.validateMessage = errors ? errors[0].message : '';

//                 callback(this.validateMessage);
//             });
//         }
//         $scope.resetField() {
//             this.validateState = '';
//             this.validateMessage = '';

//             let model = this.form.model;
//             let value = this.fieldValue;
//             let path = $scope.$ctrl.prop;
//             if (path.indexOf(':') !== -1) {
//                 path = path.replace(/:/, '.');
//             }

//             let prop = getPropByPath(model, path);

//             if (Array.isArray(value) && value.length > 0) {
//                 this.validateDisabled = true;
//                 prop.o[prop.k] = [];
//             } else if (value !== '') {
//                 this.validateDisabled = true;
//                 prop.o[prop.k] = this.initialValue;
//             }
//         }
//         $scope.getRules() {
//             var formRules = this.form.rules;
//             var selfRuels = $scope.$ctrl.rules;

//             formRules = formRules ? formRules[$scope.$ctrl.prop] : [];

//             return [].concat(selfRuels || formRules || []);
//         }
//         $scope.getFilteredRule(trigger) {
//             var rules = this.getRules();

//             return rules.filter(rule => {
//                 return !rule.trigger || rule.trigger.indexOf(trigger) !== -1;
//             });
//         }
//         $scope.onFieldBlur() {
//             this.validate('blur');
//         }
//         $scope.onFieldChange() {
//             if (this.validateDisabled) {
//                 this.validateDisabled = false;
//                 return;
//             }

//             this.validate('change');
//         }

//         //computed
//         $scope.labelStyle = function() {
//             var ret = {};
//             if (this.form.labelPosition === 'top') return ret;
//             var labelWidth = this.labelWidth || this.form.labelWidth;
//             if (labelWidth) {
//                 ret.width = labelWidth;
//             }
//             return ret;
//         }
//         $scope.contentStyle = function() {
//             var ret = {};
//             if (this.form.labelPosition === 'top' || this.form.inline) return ret;
//             var labelWidth = this.labelWidth || this.form.labelWidth;
//             if (labelWidth) {
//                 ret.marginLeft = labelWidth;
//             }
//             return ret;
//         }
//         $scope.form = function() {
//             var parent = this.$parent;
//             while (parent.$options.componentName !== 'ElForm') {
//                 parent = parent.$parent;
//             }
//             return parent;
//         }
//         $scope.fieldValue = function() {
//             var model = this.form.model;
//             if (!model || !this.prop) { return; }

//             var path = this.prop;
//             if (path.indexOf(':') !== -1) {
//                 path = path.replace(/:/, '.');
//             }

//             return getPropByPath(model, path).v;
//         }


//     }

//     angular.module('bwc.component')
//         .component('bwcFormItem', {
//             template: `<div class="el-form-item" :ng-lass="{
//                         'is-error': validateState === 'error',
//                         'is-validating': validateState === 'validating',
//                         'is-required': isRequired || $ctrl.required
//                     }">
//                         <label for="{{$ctrl.prop}}" class="el-form-item__label" style="{{labelStyle}}" ng-if="$ctrl.label">
//                         {{$ctrl.label + form().labelSuffix}}
//                         </label>
//                         <div class="el-form-item__content" style="{{contentStyle}}">
//                         <span ng-transclude></span>
//                         <div class="el-form-item__error" ng-if="validateState === 'error' && $ctrl.showMessage && form().showMessage">{{validateMessage}}</div>
//                         </div>
//                     </div>`,
//             controller: ['$scope', BwcFormItemController],
//             transclude: true,
//             bindings: {
//                 label: "@?",
//                 labelWidth: "@?",
//                 prop: "@?",
//                 required: "=?",
//                 rules: "=?",
//                 error: "@?",
//                 validateStatus: "@?",
//                 showMessage: "=?"
//             }
//         });

// })(angular);
// (function(angular) {

//     function BwcFormController($scope) {
//         $scope.$ctrl.labelSuffix = $scope.$ctrl.labelSuffix ? $scope.$ctrl.labelSuffix : ''
//         $scope.$ctrl.showMessage = $scope.$ctrl.showMessage ? $scope.$ctrl.showMessage : true

//         $scope.fields = []

//         $scope.$on('bwc.form.addField', function(field) {
//             if (field) {
//                 $scope.fields.push(field);
//             }
//         });
//         /* istanbul ignore next */
//         $scope.$on('bwc.form.removeField', function(field) {
//             if (field.prop) {
//                 $scope.fields.splice($scope.fields.indexOf(field), 1);
//             }
//         });

//         $scope.$watch('$ctrl.rules', function() {
//             $scope.validate()
//         });

//         $scope.resetFields() {
//             $scope.fields.forEach(field => {
//                 field.resetField();
//             });
//         }
//         $scope.validate(callback) {
//             let valid = true;
//             let count = 0;
//             $scope.fields.forEach((field, index) => {
//                 field.validate('', errors => {
//                     if (errors) {
//                         valid = false;
//                     }
//                     if (typeof callback === 'function' && ++count === $scope.fields.length) {
//                         callback(valid);
//                     }
//                 });
//             });
//         }
//         $scope.validateField(prop, cb) {
//             var field = $scope.fields.filter(field => field.prop === prop)[0];
//             if (!field) { throw new Error('must call validateField with valid prop string!'); }

//             field.validate('', cb);
//         }

//     }

//     angular.module('bwc.component')
//         .component('bwcForm', {
//             template: `<form class="el-form" ng-class="[
//                             $ctrl.labelPosition ? 'el-form--label-' + $ctrl.labelPosition : '',
//                             { 'el-form--inline': $ctrl.inline }
//                         ]" ng-transclude>
//                         </form>`,
//             controller: ['$scope', BwcFormController],
//             transclude: true,
//             bindings: {
//                 model: "=?",
//                 rules: "=?",
//                 labelPosition: "@?",
//                 labelWidth: "@?",
//                 labelSuffix: "@?",
//                 inline: "=?",
//                 showMessage: "=?"
//             }
//         });

// })(angular);
(function(angular) {

    function BwcColController($scope) {
        $scope.$ctrl.span = $scope.$ctrl.span ? $scope.$ctrl.span : 24

        $scope.classList = []

        var classTemp = ['span', 'offset', 'pull', 'push']
        classTemp.forEach(prop => {
            if ($scope.$ctrl[prop]) {
                $scope.classList.push(
                    prop !== 'span' ?
                    `el-col-${prop}-${$scope.$ctrl[prop]}` :
                    `el-col-${$scope.$ctrl[prop]}`
                );
            }
        });
        var classTemp = ['xs', 'sm', 'md', 'lg']
        classTemp.forEach(size => {
            if (typeof parseInt($scope.$ctrl[size]) === 'number') {
                $scope.classList.push(`el-col-${size}-${$scope.$ctrl[size]}`);
            } else if (typeof $scope.$ctrl[size] === 'object') {
                let props = $scope.$ctrl[size];
                Object.keys(props).forEach(prop => {
                    $scope.classList.push(
                        prop !== 'span' ?
                        `el-col-${size}-${prop}-${props[prop]}` :
                        `el-col-${size}-${props[prop]}`
                    );
                });
            }
        });

        var ret = {};
        if ($scope.$parent.$parent.$ctrl.gutter) {
            ret.paddingLeft = $scope.$parent.$parent.$ctrl.gutter / 2 + 'px';
            ret.paddingRight = ret.paddingLeft;
        }
        $scope.style = ret

    }

    angular.module('bwc.component')
        .component('bwcCol', {
            template: ` <div
                            class="el-col"
                            ng-class=classList
                            ng-style={{style}}
                            ng-transclude>
                        </div>`,
            controller: ['$scope', BwcColController],
            transclude: true,
            replace: true,
            bindings: {
                span: "@?",
                offset: "@?",
                pull: "@?",
                push: "@?",
                xs: "@?",
                sm: "@?",
                md: "@?",
                lg: "@?"
            }
        });

})(angular);
(function(angular) {

    function BwcRowController($scope) {
        $scope.$ctrl.justify = $scope.$ctrl.justify ? $scope.$ctrl.justify : 'start'
        $scope.$ctrl.align = $scope.$ctrl.align ? $scope.$ctrl.align : 'top'

        //style
        var ret = {};
        if ($scope.$ctrl.gutter) {
            ret.marginLeft = `-${$scope.$ctrl.gutter / 2}px`;
            ret.marginRight = ret.marginLeft;
        }
        $scope.style = ret
    }

    angular.module('bwc.component')
        .component('bwcRow', {
            template: `<div
                            class="el-row"
                            style="{{style}}"
                            ng-class="[
                            $ctrl.justify !== 'start' ? 'is-justify-' + $ctrl.justify : '',
                            $ctrl.align !== 'top' ? 'is-align-' + $ctrl.align : '',
                            {
                                'el-row--flex': $ctrl.type === 'flex'
                            }
                            ]"
                            ng-transclude
                        >
                        </div>`,
            controller: ['$scope', BwcRowController],
            transclude: true,
            replace: true,
            bindings: {
                gutter: "@?",
                type: "@?",
                justify: "@?",
                align: "@?"
            }
        });

})(angular);
(function(angular) {

    function BwcProgressController($scope) {
        $scope.$ctrl.type = $scope.$ctrl.type ? $scope.$ctrl.type : 'line'
        var typeArray = ['line', 'circle']
        if ($scope.$ctrl.type != 'line' && $scope.$ctrl.type != 'circle') {
            return
        }
        
        $scope.$ctrl.percentage = $scope.$ctrl.percentage ? $scope.$ctrl.percentage : 0
        if ($scope.$ctrl.percentage < 0) {
            $scope.$ctrl.percentage = 0
        }
        if ($scope.$ctrl.percentage > 100) {
            $scope.$ctrl.percentage = 100
        }
        $scope.$ctrl.strokeWidth = $scope.$ctrl.strokeWidth ? $scope.$ctrl.strokeWidth : 6
        $scope.$ctrl.textInside = $scope.$ctrl.textInside ? $scope.$ctrl.textInside : false
        $scope.$ctrl.width = $scope.$ctrl.width ? $scope.$ctrl.width : 126
        $scope.$ctrl.showText = $scope.$ctrl.showText ? $scope.$ctrl.showText : true

        /**
         * mehtods
         */
        $scope.barStyle = function() {
            var style = {};
            style.width = $scope.$ctrl.percentage + '%';
            return style;
        }
        $scope.relativeStrokeWidth = function() {
            return ($scope.$ctrl.strokeWidth / $scope.$ctrl.width * 100).toFixed(1);
        }
        $scope.trackPath = function() {
            var radius = parseInt(50 - parseFloat($scope.relativeStrokeWidth()) / 2, 10);
            return `M 50 50 m 0 -${radius} a ${radius} ${radius} 0 1 1 0 ${radius * 2} a ${radius} ${radius} 0 1 1 0 -${radius * 2}`;
        }
        $scope.perimeter = function() {
            var radius = 50 - parseFloat($scope.relativeStrokeWidth()) / 2;
            return 2 * Math.PI * radius;
        }
        $scope.circlePathStyle = function() {
            var perimeter = $scope.perimeter()
            return {
                strokeDasharray: `${perimeter}px,${perimeter}px`,
                strokeDashoffset: (1 - $scope.$ctrl.percentage / 100) * perimeter + 'px',
                transition: 'stroke-dashoffset 0.6s ease 0s, stroke 0.6s ease'
            };
        }
        $scope.stroke = function() {
            var ret;
            switch ($scope.$ctrl.status) {
                case 'success':
                    ret = '#13ce66';
                    break;
                case 'exception':
                    ret = '#ff4949';
                    break;
                default:
                    ret = '#20a0ff';
            }
            return ret;
        }
        $scope.iconClass = function() {
            if ($scope.$ctrl.type === 'line') {
                return $scope.$ctrl.status === 'success' ? 'el-icon-circle-check' : 'el-icon-circle-cross';
            } else {
                return $scope.$ctrl.status === 'success' ? 'el-icon-check' : 'el-icon-close';
            }
        }
        $scope.progressTextSize = function() {
            return $scope.$ctrl.type === 'line' ?
                12 + $scope.$ctrl.strokeWidth * 0.4 :
                $scope.$ctrl.width * 0.111111 + 2;
        }
    }

    angular.module('bwc.component')
        .directive('svgD', function() {
            return {
                restrict: 'A',
                link: function(scope, element, attrs) {
                    element.attr('d', attrs.svgD)
                }
            };
        });

    angular.module('bwc.component')
        .component('bwcProgress', {
            template: `<div
                        class="el-progress"
                        ng-class="[
                        'el-progress--' + $ctrl.type,
                        $ctrl.status ? 'is-' + $ctrl.status : '',
                        {
                            'el-progress--without-text': !$ctrl.showText,
                            'el-progress--text-inside': $ctrl.textInside,
                        }
                        ]"
                    >
                        <div class="el-progress-bar" ng-if="$ctrl.type === 'line'">
                            <div class="el-progress-bar__outer" ng-style="{height: $ctrl.strokeWidth + 'px'}">
                                <div class="el-progress-bar__inner" ng-style="barStyle()">
                                <div class="el-progress-bar__innerText" ng-if="$ctrl.showText && $ctrl.textInside">{{$ctrl.percentage}}%</div>
                                </div>
                            </div>
                        </div>
                        <div ng-if="$ctrl.type !== 'line'" class="el-progress-circle" ng-style="{height: $ctrl.width + 'px', width: $ctrl.width + 'px'}" >
                            <svg viewBox="0 0 100 100">
                                <path class="el-progress-circle__track" svg-d="{{trackPath()}}" stroke="#e5e9f2" stroke-width="{{relativeStrokeWidth()}}" fill="none"></path>
                                <path class="el-progress-circle__path" svg-d="{{trackPath()}}" stroke-linecap="round" stroke="{{stroke()}}" stroke-width="{{relativeStrokeWidth()}}" fill="none" ng-style="circlePathStyle()"></path>
                            </svg>
                        </div>
                        <div
                        class="el-progress__text"
                        ng-if="$ctrl.showText && !$ctrl.textInside"
                        ng-style="{fontSize: progressTextSize() + 'px'}"
                        >
                            <span ng-if="!$ctrl.status">{{$ctrl.percentage}}%</span>
                            <i ng-if="$ctrl.status" ng-class="iconClass()"></i>
                        </div>
                    </div>`,
            controller: ['$scope', BwcProgressController],
            transclude: true,
            replace: true,
            bindings: {
                type: "@?",
                percentage: "=?",
                status: "@?",
                strokeWidth: "=?",
                textInside: "=?",
                width: "=?",
                showText: "=?"
            }
        });

})(angular);
(function(angular) {

    function BwcRadioController($scope) {
        console.log($scope.$ctrl)
        $scope.$ctrl.value = $scope.$ctrl.value ? $scope.$ctrl.value : {}
        $scope.$ctrl.label = $scope.$ctrl.label ? $scope.$ctrl.label : {}
    }

    angular.module('bwc.component')
        .component('bwcRadio', {
            template: `<label class="el-radio">
						<span class="el-radio__input"
						ng-class="{
							'is-disabled': $ctrl.isDisabled,
							'is-checked': $ctrl.bwcModel === $ctrl.label,
							'is-focus': focus
						}"
						>
						<span class="el-radio__inner"></span>
						<input
							class="el-radio__original"
							ng-value="$ctrl.label"
							type="radio"
							ng-model="$ctrl.bwcModel"
							ng-focus="focus = true"
							ng-blur="focus = false"
							name="{{$ctrl.name}}"
							>
						</span>
						<span class="el-radio__label">
						<span ng-transclude></span>
						</span>
					</label>`,
            controller: ['$scope', BwcRadioController],
            transclude: true,
            replace: true,
            bindings: {
                value: "=?",
                label: "=?",
                disabled: "=?",
                name: "@?",
                bwcModel: "=?"
            }
        });

})(angular);
(function(angular) {

    function BwcUploadDraggerController($scope) {
        $scope.dragover = false

        /**
         * mehtods
         */
        window.onDrop = function(e) {
            e.preventDefault();
            e.returnValue = false;
            $scope.dragover = false;
            $scope.$ctrl.onFile(e.dataTransfer.files)
        }

        window.dragoverFn = function(e, flag) {
            e.preventDefault();
            e.returnValue = false;
            $scope.dragover = flag
        }

    }

    angular.module('bwc.component')
        .component('bwcUploadDragger', {
            template: `<div
                            class="el-upload-dragger"
                            ng-class="{
                                'is-dragover': dragover
                            }"
                            ondrop="onDrop(event)"
                            ondragover="dragoverFn(event,true)"
                            ondragleave="dragoverFn(event,false)"
                            ng-transclude
                        >
                        </div>`,
            controller: ['$scope', BwcUploadDraggerController],
            transclude: true,
            replace: true,
            bindings: {
                onFile: '=?'
            }
        });

})(angular);
(function(angular) {

    function BwcUploadListController($scope) {
        $scope.$ctrl.files = $scope.$ctrl.files ? $scope.$ctrl.files : []
            /**
             * mehtods
             */
        $scope.parsePercentage = function(val) {
            return parseInt(val, 10);
        }
        $scope.handleClick = function(file) {
            $scope.$ctrl.handlePreview && $scope.$ctrl.handlePreview(file);
        }

        $scope.type = $scope.$ctrl.listType === 'picture-card' ? 'circle' : 'line'
        $scope.strokeWidth = $scope.$ctrl.listType === 'picture-card' ? 6 : 2

    }

    angular.module('bwc.component')
        .component('bwcUploadList', {
            template: `<ul ng-class="['el-upload-list', 'el-upload-list--' + $ctrl.listType]">
                            <li
                                ng-repeat="file in $ctrl.files"
                                ng-class="['el-upload-list__item', 'is-' + file.status]"
                                key="{{file}}"
                            >
                            <img
                                class="el-upload-list__item-thumbnail"
                                ng-if="['picture-card', 'picture'].indexOf($ctrl.listType) > -1 && file.status === 'success'"
                                ng-src="{{file.url}}" alt=""
                            >
                            <a class="el-upload-list__item-name" ng-click="handleClick(file)">
                                <i class="el-icon-document"></i>{{file.name}}
                            </a>
                            <label ng-show="file.status === 'success'" class="el-upload-list__item-status-label">
                                <i ng-class="{
                                    'el-icon-circle-check': $ctrl.listType === 'text',
                                    'el-icon-check': ['picture-card', 'picture'].indexOf($ctrl.listType) > -1
                                }"></i>
                                <i class="el-icon-close" ng-click="$ctrl.handleRemove(file)"></i>
                            </label>
                            <span class="el-upload-list__item-actions"
                                ng-if="
                                $ctrl.listType === 'picture-card' &&
                                file.status === 'success'
                                "
                            >
                                <span
                                ng-if="
                                    $ctrl.handlePreview &&
                                    $ctrl.listType === 'picture-card'
                                "
                                ng-click="$ctrl.handlePreview(file)"
                                class="el-upload-list__item-preview"
                                >
                                <i class="el-icon-view"></i>
                                </span>
                                <span
                                class="el-upload-list__item-delete"
                                ng-click="$ctrl.handleRemove(file)"
                                >
                                <i class="el-icon-delete2"></i>
                                </span>
                            </span>
                            <bwc-progress
                                ng-if="file.status === 'uploading'"
                                type="{{type}}"
                                stroke-width="strokeWidth"
                                percentage="parsePercentage(file.percentage)">
                            </bwc-progress>
                            </li>
                        </ul>`,
            controller: ['$scope', BwcUploadListController],
            transclude: true,
            replace: true,
            bindings: {
                files: "=?",
                handlePreview: "=?",
                handleRemove: "=?",
                listType: "@?",
            }
        });

})(angular);
(function(angular) {

    function BwcUploadController($scope, $element) {

        //init default data
        function noop() {}
        $scope.$ctrl.headers = $scope.$ctrl.headers ? $scope.$ctrl.headers : {}
        $scope.$ctrl.name = $scope.$ctrl.name ? $scope.$ctrl.name : 'file'
        $scope.$ctrl.showFileList = $scope.$ctrl.showFileList ? $scope.$ctrl.showFileList : true
        $scope.$ctrl.type = $scope.$ctrl.type ? $scope.$ctrl.type : 'select'
        $scope.$ctrl.onRemove = $scope.$ctrl.onRemove ? $scope.$ctrl.onRemove : noop
        $scope.$ctrl.onChange = $scope.$ctrl.onChange ? $scope.$ctrl.onChange : noop
        $scope.$ctrl.onSuccess = $scope.$ctrl.onSuccess ? $scope.$ctrl.onSuccess : noop
        $scope.$ctrl.onProgress = $scope.$ctrl.onProgress ? $scope.$ctrl.onProgress : noop
        $scope.$ctrl.onError = $scope.$ctrl.onError ? $scope.$ctrl.onError : noop
        $scope.$ctrl.fileList = $scope.$ctrl.fileList ? $scope.$ctrl.fileList : []
        $scope.$ctrl.autoUpload = $scope.$ctrl.autoUpload ? $scope.$ctrl.autoUpload : true
        $scope.$ctrl.listType = $scope.$ctrl.listType ? $scope.$ctrl.listType : 'text'

        //init data end

        /**
         * data
         */
        $scope.uploadFiles = []
        $scope.dragOver = false
        $scope.draging = false
        $scope.tempIndex = 1

        if ($scope.$ctrl.fileList.length > 0) {
            for (var i = 0; i < $scope.$ctrl.fileList.length; i++) {
                $scope.uploadFiles.push({
                    uid: $scope.$ctrl.fileList[i].uid || (Date.now() + $scope.tempIndex++),
                    status: 'success',
                    name: $scope.$ctrl.fileList[i].name,
                    url: $scope.$ctrl.fileList[i].url
                })
            }
        }

        /**
         * method
         */
        $scope.handleProgress = function(ev, rawFile) {
            var file = $scope.getFile(rawFile);
            $scope.$ctrl.onProgress(ev, file, $scope.uploadFiles);
            file.status = 'uploading';
            file.percentage = ev.percent || 0;
        }
        $scope.handleSuccess = function(res, rawFile) {
            var file = $scope.getFile(rawFile);

            if (file) {
                file.status = 'success';
                file.response = res;
                $scope.$ctrl.onSuccess(res, file, $scope.uploadFiles);
                $scope.$ctrl.onChange(file, $scope.uploadFiles);
            }
        }
        $scope.handleError = function(err, rawFile) {
            var file = $scope.getFile(rawFile);
            var fileList = $scope.uploadFiles;

            file.status = 'fail';

            fileList.splice(fileList.indexOf(file), 1);

            $scope.$ctrl.onError(err, file, $scope.uploadFiles);
            $scope.$ctrl.onChange(file, $scope.uploadFiles);
        }
        $scope.handleRemove = function(file) {
            var fileList = $scope.uploadFiles;
            fileList.splice(fileList.indexOf(file), 1);
            $scope.$ctrl.onRemove(file, fileList);
        }
        $scope.getFile = function(rawFile) {
            var fileList = $scope.uploadFiles;
            var target;
            fileList.every(item => {
                target = rawFile.uid === item.uid ? item : null;
                return !target;
            });
            return target;
        }
        $scope.clearFiles = function() {
            this.uploadFiles = [];
        }
        $scope.submit = function() {
            this.uploadFiles
                .filter(file => file.status === 'ready')
                .forEach(file => {
                    this.$refs['upload-inner'].upload(file.raw, file);
                });
        }
        $scope.handleClick = function(e) {
            setTimeout(function() {
                angular.element(e.currentTarget).children().click()
            }, 1)
        }
        $scope.handleChange = function(ev) {
            const files = ev.files;
            if (!files) return;
            $scope.uploadFilesFn(files);
            angular.element(ev)[0].value = null
            $scope.file = null;
            $scope.$apply()
        }

        $scope.uploadFilesFn = function(files) {
            let postFiles = Array.prototype.slice.call(files);
            if (!$scope.$ctrl.multiple) { postFiles = postFiles.slice(0, 1); }

            if (postFiles.length === 0) { return; }

            postFiles.forEach(rawFile => {
                if (!$scope.$ctrl.thumbnailMode || $scope.isImage(rawFile.type)) {
                    $scope.onStart(rawFile);
                    if ($scope.$ctrl.autoUpload) $scope.upload(rawFile);
                }
            });
        }
        $scope.isImage = function(str) {
            return str.indexOf('image') !== -1;
        }
        $scope.onStart = function(rawFile) {
            rawFile.uid = Date.now() + this.tempIndex++;
            let file = {
                status: 'ready',
                name: rawFile.name,
                size: rawFile.size,
                percentage: 0,
                uid: rawFile.uid,
                raw: rawFile
            };

            try {
                file.url = URL.createObjectURL(rawFile);
            } catch (err) {
                console.error(err);
                return;
            }

            $scope.uploadFiles.push(file);
        }

        $scope.upload = function(rawFile, file) {
            if (!$scope.$ctrl.beforeUpload) {
                return post(rawFile);
            }

            const before = $scope.$ctrl.beforeUpload(rawFile);
            if (before && before.then) {
                before.then(processedFile => {
                    if (Object.prototype.toString.call(processedFile) === '[object File]') {
                        post(processedFile);
                    } else {
                        post(rawFile);
                    }
                }, () => {
                    $scope.$ctrl.onRemove(rawFile, true);
                });
            } else if (before !== false) {
                post(rawFile);
            } else {
                $scope.$ctrl.onRemove(rawFile, true);
            }
        }

        function post(rawFile) {
            ajax({
                headers: $scope.$ctrl.headers,
                withCredentials: $scope.$ctrl.withCredentials,
                file: rawFile,
                data: $scope.$ctrl.data,
                filename: $scope.$ctrl.name,
                action: $scope.$ctrl.action,
                onProgress: e => {
                    $scope.handleProgress(e, rawFile);
                    $scope.$apply();
                },
                onSuccess: res => {
                    $scope.handleSuccess(res, rawFile);
                    $scope.$apply();
                },
                onError: err => {
                    $scope.handleError(err, rawFile);
                    $scope.$apply();
                }
            });
        }

        function ajax(option) {
            if (typeof XMLHttpRequest === 'undefined') {
                return;
            }

            const xhr = new XMLHttpRequest();
            const action = option.action;

            if (xhr.upload) {
                xhr.upload.onprogress = function progress(e) {
                    if (e.total > 0) {
                        e.percent = e.loaded / e.total * 100;
                    }
                    option.onProgress(e);
                };
            }

            const formData = new FormData();

            if (option.data) {
                Object.keys(option.data).map(key => {
                    formData.append(key, option.data[key]);
                });
            }

            formData.append(option.filename, option.file);

            xhr.onerror = function error(e) {
                option.onError(e);
            };

            xhr.onload = function onload() {
                if (xhr.status < 200 || xhr.status >= 300) {
                    return option.onError(getError(action, option, xhr));
                }

                option.onSuccess(getBody(xhr));
            };

            xhr.open('post', action, true);

            if (option.withCredentials && 'withCredentials' in xhr) {
                xhr.withCredentials = true;
            }

            const headers = option.headers || {};

            for (let item in headers) {
                if (headers.hasOwnProperty(item) && headers[item] !== null) {
                    xhr.setRequestHeader(item, headers[item]);
                }
            }
            xhr.send(formData);
            return xhr;
        }

        function getError(action, option, xhr) {
            let msg;
            if (xhr.response) {
                msg = `${xhr.status} ${xhr.response.error || xhr.response}`;
            } else if (xhr.responseText) {
                msg = `${xhr.status} ${xhr.responseText}`;
            } else {
                msg = `fail to post ${action} ${xhr.status}'`;
            }

            const err = new Error(msg);
            err.status = xhr.status;
            err.method = 'post';
            err.url = action;
            return err;
        }

        function getBody(xhr) {
            const text = xhr.responseText || xhr.response;
            if (!text) {
                return text;
            }

            try {
                return JSON.parse(text);
            } catch (e) {
                return text;
            }
        }
    }

    angular.module('bwc.component')
        .component('bwcUpload', {
            template: `<div>
                            <span ng-if="$ctrl.listType === 'picture-card'">
                                <bwc-upload-list ng-if="$ctrl.showFileList" list-type='{{$ctrl.listType}}' files='uploadFiles' handle-remove='handleRemove' handle-preview='$ctrl.onPreview'>
                                </bwc-upload-list>
                            </span>
                            <div class="el-upload" ng-class="['el-upload--'+$ctrl.listType]" ng-click="handleClick($event)">
                                <div ng-if="$ctrl.drag">
                                    <bwc-upload-dragger on-file="uploadFilesFn"> <span ng-transclude></span></bwc-upload-dragger>
                                </div>
                                <span ng-if="!$ctrl.drag" ng-transclude></span>
                                <input class="el-upload__input" type="file" ng-model="file" onchange="angular.element(this).scope().handleChange(this)" multiple="{{$ctrl.multiple}}" accept="{{$ctrl.accept}}"></input>
                            </div>
                            <span ng-transclude="tip"></span>
                            <span ng-if="$ctrl.listType !== 'picture-card'">
                                <bwc-upload-list ng-if="$ctrl.showFileList" list-type='{{$ctrl.listType}}' files='uploadFiles' handle-remove='handleRemove' handle-preview='$ctrl.onPreview'>
                                </bwc-upload-list>
                            </span>
                        </div>
                        `,
            controller: ['$scope', BwcUploadController],
            transclude: {
                tip: "?tip"
            },
            replace: true,
            bindings: {
                action: "@",
                headers: "=?",
                data: "=?",
                multiple: "=?",
                name: "@?",
                drag: "<?",
                dragger: "=?",
                withCredentials: "=?",
                showFileList: "<?",
                accept: "@?",
                type: "@?",
                beforeUpload: "=?",
                onRemove: "=?",
                onChange: "=?",
                onPreview: "=?",
                onSuccess: "=?",
                onProgress: "=?",
                onError: "=?",
                fileList: "=?",
                autoUpload: "<?",
                listType: "@?",
            }
        });

})(angular);