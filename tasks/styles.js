'use strict';
var gulp = require('gulp');
var postcss = require('gulp-postcss');
var cssmin = require('gulp-cssmin');
var salad = require('postcss-salad')(require('../salad.config.json'));

module.exports = function(gulp, $) {

    gulp.task('styles:copy-source', function() {
        return gulp.src('source/theme-default/font/*.*')
            .pipe(gulp.dest('dist'));
    });

    gulp.task('styles', function() {
        return gulp.src('source/theme-default/**/*.css')
            .pipe($.cssnano({ safe: true }))
            .pipe($.rename('bwc-component.min.css'))
            .pipe(gulp.dest('dist'));
    });


    gulp.task('compile', function() {
        return gulp.src('source/theme-default/src/*.css')
            .pipe(postcss([salad]))
            .pipe(cssmin())
            .pipe(gulp.dest('dist/theme-default'));
    });

    gulp.task('copyfont', function() {
        return gulp.src('source/theme-default/src/fonts/**')
            .pipe(cssmin())
            .pipe(gulp.dest('dist/theme-default/fonts'));
    });

    gulp.task('build:css', ['compile', 'copyfont']);


};